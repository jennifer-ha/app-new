/*
  Warnings:

  - You are about to drop the column `gebruikerEmail` on the `Alarm` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Alarm" DROP COLUMN "gebruikerEmail";
